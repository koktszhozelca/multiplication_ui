COMP2222 Assignment 1

Animation
    https://www.npmjs.com/package/react-animations

A web interface for teaching the multiplication table.
    Teacher interface
    Student interface

Idea
The students in a class are divided into several groups.

For each group, there is an account for logging in the system, which
is used to train their robot by answering questions.

They may answer the question thourgh discussion, then the performance of the
robot is depending on the accuracy of the answer.

The teacher will ask question to their robot, and their robot will reply the
corresponding answer that trained before.

The Teacher interface shows how well do the group of students study.
