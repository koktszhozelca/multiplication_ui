import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import Auth from './Auth';
import Train from './Train';
import Login from './Login';
import Menu from './Menu';
import Learning from './Learning';
import RobotManagement from './RobotManagement';
import Performance from './Performance';
import Settings from './Settings';

class Main extends Component {
  constructor(props){
    super(props);
    this.state = {
      isLoggedIn : null
    }
  }

  async componentWillMount(){
    this.setState({isLoggedIn: await Auth.isLoggedIn()})
  }

  componentDidMount(){

  }

  componentWillUnMount(){

  }

  render(){
    return (
      this.state.isLoggedIn ?
        <Router>
          <Switch>
            <Route exact path="/Learning" render={()=>(<Learning/>)}/>
            <Route exact path="/Robot-Management" render={()=>(<RobotManagement/>)}/>
            <Route exact path="/Performance" render={()=>(<Performance/>)}/>
            <Route exact path="/Settings" render={()=>(<Settings/>)}/>
            <Route path="/" render={()=>(<Menu/>)}/>
          </Switch>
        </Router>
       : this.state.isLoggedIn === null ? null : <Login/>
    )
  }
}
export default Main;
