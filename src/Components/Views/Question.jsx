import React, {Component} from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';
import MobileStepper from '@material-ui/core/MobileStepper';
import { merge, zoomIn, zoomOut, fadeOut, shake } from 'react-animations';
import Radium, {StyleRoot} from 'radium';
import CardOption from './CardOption';

class Question extends Component {
  constructor(props){
    super(props);
    this.state = {
      questions: props.questions,
      answers: new Object(),
      index: 0
    }
  }

  resize=()=>this.forceUpdate();

  componentWillMount(){
    this.resize();
  }

  componentDidMount(){
    window.addEventListener('resize', this.resize);
  }

  componentWillUnMount(){
    window.removeEventListener('resize', this.resize);
  }

  prev=()=>{
    this.setState({index: this.state.index===0 ? 0 : this.state.index-1});
  }

  next=()=>{
    this.setState({index: this.state.index>=this.state.questions.length-1? this.state.questions.length-1 : this.state.index+1});
  }

  getNextBtnColor=()=>{
    return this.state.index>=this.state.questions.length-1? "rgb(255, 205, 182)" : "rgb(255, 121, 62)";
  }

  getPrevBtnColor=()=>{
    return this.state.index===0? "rgb(255, 205, 182)" : "rgb(255, 121, 62)";
  }

  setAnswer=(index, ans)=>{
    var answers = this.state.answers;
    answers[index]=ans;
    this.setState({answers: answers});
    console.log("ANS", this.state.answers[this.state.index]);
  }

  render(){
    var options = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
    var isAnswered = typeof(this.state.answers[this.state.index]) !== "undefined";
    console.log(isAnswered, this.state.index);
    return(
      <div style={{textAlign:"center", margin:"20px"}}>
        <div style={{textAlign:"center", fontSize:"100px"}}>
          {this.state.questions[this.state.index] + (isAnswered ? this.state.answers[this.state.index] : "?")}
        </div>
        <div style={{textAlign:"center", fontSize:"70px", marginTop:"28%"}}>
          <Grid container direction="row" justify={window.innerWidth > 1000 ? "space-evenly" : "center"} alignItems="center" spacing={8}>
            {
              options.map((num)=>{
                return (
                  <Grid item key={this.state.index+num}>
                    <CardOption content={num} handler={this} index={this.state.index}/>
                  </Grid>
                )
              })
            }
          </Grid>
        </div>
        <MobileStepper
          style={{marginTop:"2%", backgroundColor:"white"}}
          variant="dots"
          steps={this.state.questions.length}
          position="static"
          activeStep={this.state.index}
          nextButton={
            <Fab style={{backgroundColor:this.getNextBtnColor(), color:"white"}} onClick={this.next}>
              <Icon>keyboard_arrow_right</Icon>
            </Fab>
          }
          backButton={
            <Fab style={{backgroundColor:this.getPrevBtnColor(), color:"white"}} onClick={this.prev}>
              <Icon>keyboard_arrow_left</Icon>
            </Fab>
          }
        />
      </div>
    )
  }
}
export default Question;
