import React, {Component} from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { merge, pulse } from 'react-animations';
import Radium, {StyleRoot} from 'radium';

const ANIM_SELECT = {
  animation: 'x 0.2s',
  animationName: Radium.keyframes(pulse, 'headShake'),
}

const ItemStyle = {
  cursor: "pointer",
  height: 250
}

const ItemImg = {
  height: window.innerWidth>1000? 200 : window.innerHeight * 0.25,
  width: "auto",
  margin: window.innerWidth>1000? "auto" : "8% auto",
  float: "none"
}

class CardItem extends Component {
  constructor(props){
    super(props);
    this.state = {
      stage: null,
      img: props.img,
      title: props.title,
      isHover: false
    }
    this.handler = props.handler;
  }

  resize=()=>this.forceUpdate();

  componentWillMount(){
    this.resize();
  }

  componentDidMount(){
    window.addEventListener('resize', this.resize);
  }

  componentWillUnMount(){
    window.removeEventListener('resize', this.resize);
  }

  select=()=>{
    this.setState({stage: ANIM_SELECT}, ()=>{
      setTimeout(()=>{
        this.setState({stage: null}, ()=>{
          this.handler.navigate(this.state.title)
        })
      }, 200);
    })
  }

  hover=()=>{
    this.setState({isHover:true})
  }

  blur=()=>{
    this.setState({isHover:false})
  }

  render(){
    return(
      <StyleRoot>
        <div style={this.state.stage}>
          <Card raised={this.state.isHover} style={ItemStyle} onClick={this.select} onMouseOver={this.hover} onMouseLeave={this.blur}>
            <CardMedia style={ItemImg} component="img" image={this.state.img}/>
            <CardContent>
              <Typography variant="subheader" style={{fontWeight:"bold"}}>{this.state.title}</Typography>
            </CardContent>
          </Card>
        </div>
      </StyleRoot>
    )
  }
}
export default CardItem;
