import React, {Component} from 'react';
import Card from '@material-ui/core/Card';

class CardOption extends Component {
  constructor(props){
    super(props);
    this.state = {
      content: props.content,
      isHover: false
    }
    this.index = props.index;
    this.handler = props.handler;
  }

  resize=()=>this.forceUpdate();

  componentWillMount(){
    this.resize();
  }

  componentDidMount(){
    window.addEventListener('resize', this.resize);
  }

  componentWillUnMount(){
    window.removeEventListener('resize', this.resize);
  }

  hover=()=>{
    this.setState({isHover: true});
  }

  blur=()=>{
    this.setState({isHover: false});
  }

  click=()=>{
    this.handler.setAnswer(this.index, this.state.content)
  }

  render(){
    return(
      <Card raised={this.state.isHover}
            onClick={this.click}
            onMouseOver={this.hover}
            onMouseLeave={this.blur}
            style={{width: window.innerWidth > 1000 ? 80 : 60, textAlign:"center", cursor:"pointer"}}>{this.state.content}</Card>
    )
  }
}
export default CardOption;
