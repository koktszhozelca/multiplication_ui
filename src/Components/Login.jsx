import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import LoginIcon from '@material-ui/icons/VpnKey';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import { merge, zoomIn, zoomOut, fadeOut, shake } from 'react-animations';
import Radium, {StyleRoot} from 'radium';

import Auth from './Auth';
import LayersLogo from './layers.png';

import "../App.css";

const ANIM_EXIT = merge(fadeOut, zoomOut);

const ANIM_IN = {
  animation: 'x 0.5s',
  animationName: Radium.keyframes(zoomIn, 'zoomIn')
}

const ANIM_OUT = {
  animation: 'x 0.5s',
  animationName: Radium.keyframes(ANIM_EXIT, 'ANIM_EXIT')
}

const ANIM_ERR = {
  animation: 'x 0.5s',
  animationName: Radium.keyframes(shake, 'shake'),
}

class Login extends Component {
  constructor(props){
    super(props);
    this.state = {
      stage: ANIM_IN,
      email: "",
      password:""
    }
    this.cardStyle = {
      width: window.innerWidth*0.9+"px",
      maxWidth: "500px",
      height:"auto",
      margin:"auto",
      float:"none",
      marginTop:"5%",
    }
    this.avatarStyle = {
      margin:10,
      backgroundColor: "rgb(64, 182, 154)",
    }
  }

  resize=()=>{
    this.setState({width: window.innerWidth*0.9+"px"});
  }

  componentWillMount(){
    this.resize();
  }

  componentDidMount(){
    window.addEventListener("resize", this.resize);
  }

  componentWillUnMount(){
    window.removeEventListener("resize", this.resize);
  }

  updateEmail=(e)=>{
    this.setState({email: e.target.value});
  }

  updatePassword=(e)=>{
    this.setState({password: e.target.value});
  }

  async login(e){
    e.preventDefault();
    await Auth.emailAuth(this.state.email, this.state.password)
      .then((cred)=>this.exit())
      .catch((err)=>this.setState({stage:ANIM_ERR},()=>setTimeout(()=>this.setState({stage:null}),300)))
  }

  exit=()=>{
    this.setState({stage:ANIM_OUT}, ()=>{
      setTimeout(()=>{
        this.setState({stage:{display:"none"}}, ()=>{
          window.location = "/"
        })
      }, 300);
    });
  }

  render(){
    return (
      <StyleRoot>
        <div style={this.state.stage}>
          <Card style={this.cardStyle}>
            <div style={{margin:"auto", float:"none", textAlign:"center"}}>
              <img src={LayersLogo} style={{width: 200, height:"auto", marginTop:"1%"}}/>
              <div className="brandName">Multiplication Tutor</div>
            </div>

                      <form action="#">
            <CardContent>
                <TextField
                  id="email"
                  label="Email Address"
                  variant="outlined"
                  value={this.state.email}
                  onChange={this.updateEmail}
                  fullWidth
                />
                <TextField
                  id="password"
                  label="Password"
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  value={this.state.password}
                  onChange={this.updatePassword}
                  type="password"
                  autoComplete="current-password"
                />

            </CardContent>
            <CardActions>
              <Button variant="outlined"
                      onClick={(e)=>this.login(e)}
                      type="submit"
                      style={{color:"rgb(255, 255, 255)", backgroundColor:"rgb(255, 115, 88)"}}
                      fullWidth>
                <div style={{fontWeight:"bold"}}>LOGIN</div>
              </Button>
            </CardActions>
            </form>
          </Card>
        </div>
      </StyleRoot>
    )
  }
}
export default Login;
