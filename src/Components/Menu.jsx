import React, {Component} from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TutorIcon from '@material-ui/icons/Layers';

import { merge, zoomIn, zoomOut, fadeOut, shake, jello } from 'react-animations';
import Radium, {StyleRoot} from 'radium';

import LayersLogo from './layers.png';
import Auth from './Auth';

import CardItem from './Views/CardItem';

const ANIM_EXIT = merge(fadeOut, zoomOut);

const ANIM_IN = {
  animation: 'x 0.3s',
  animationName: Radium.keyframes(zoomIn, 'zoomIn')
}

const ANIM_OUT = {
  animation: 'x 0.5s',
  animationName: Radium.keyframes(ANIM_EXIT, 'ANIM_EXIT')
}

const ANIM_ERR = {
  animation: 'x 0.5s',
  animationName: Radium.keyframes(shake, 'shake'),
}

const AvatarStyle = {
  width: 60,
  height:60,
  backgroundColor: "rgb(233, 170, 142)"
}

const CardStyle = {
  margin:"auto",
  marginTop: "2%",
  maxWidth:"1000px",
  float: "none"
}

class Menu extends Component {
  constructor(props){
    super(props);
    this.state = {
      stage: ANIM_IN
    }
  }

  resize=()=>this.forceUpdate();

  componentWillMount(){
    this.resize();
  }

  componentDidMount(){
    window.addEventListener('resize', this.resize);
  }

  componentWillUnMount(){
    window.removeEventListener('resize', this.resize);
  }

  logout=()=>{
    Auth.logout().then(()=>{
      this.exit();
    });
  }

  navigate=(page)=>{
    this.setState({stage:ANIM_OUT}, ()=>{
      setTimeout(()=>{
        this.setState({stage:{display:"none"}}, ()=>{
          window.location = "/"+page.replace(" ", "-")
        })
      }, 300);
    });
  }

  exit=()=>{
    this.setState({stage:ANIM_OUT}, ()=>{
      setTimeout(()=>{
        this.setState({stage:{display:"none"}}, ()=>{
          window.location = "/"
        })
      }, 300);
    });
  }

  render(){
    return(
      <StyleRoot>
        <div style={this.state.stage}>
          <Card style={CardStyle}>
            <CardHeader title={
                <Grid container direction="row" justify="space-between">
                  <Grid item xs={6}>
                    <div className="menuTitle">Multiplication Tutor</div>
                  </Grid>
                  <Grid item xs={6} style={{textAlign:"right"}}>
                    <Button variant="outlined"
                            color="secondary"
                            onClick={this.logout}>Logout</Button>
                  </Grid>
                </Grid>
              }
              subheader={<span>An interactive way to learn.</span>}
              avatar={
                <Avatar style={AvatarStyle}>
                  <img src={LayersLogo} style={{width:45, height:"auto"}}/>
                </Avatar>
              }/>
            <CardContent style={{textAlign:"center"}}>
              <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
                spacing={16}
              >
                <Grid item xs={6}>
                  <CardItem handler={this}
                    title="Learning"
                    img="https://cdn.kastatic.org/ka-perseus-images/45a518b64df15c3b0319e2b00b5f6fd4edb7f009.png"/>
                </Grid>
                <Grid item xs={6}>
                  <CardItem handler={this}
                    title="Robot Management"
                    img="https://upload.wikimedia.org/wikipedia/commons/3/38/Robot-clip-art-book-covers-feJCV3-clipart.png"/>
                </Grid>
                <Grid item xs={6}>
                  <CardItem handler={this}
                    title="Performance"
                    img="https://d3frsattnbx5l6.cloudfront.net/1544957431448-speedometer-speedometer.png"/>
                </Grid>
                <Grid item xs={6}>
                  <CardItem handler={this}
                    title="Settings"
                    img="http://cartonomy.com/wp-content/uploads/2014/10/icon-setup.png"/>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </div>
      </StyleRoot>
    )
  }
}
export default Menu;
