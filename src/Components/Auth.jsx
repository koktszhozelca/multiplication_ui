import firebase from 'firebase';

const CONFIG = {
  apiKey: "AIzaSyBauBXOIJ0PD4R6WHbEGUqaViM2Kvv6Ty8",
  authDomain: "mulitplication-ac9af.firebaseapp.com",
  databaseURL: "https://mulitplication-ac9af.firebaseio.com",
  projectId: "mulitplication-ac9af",
  storageBucket: "mulitplication-ac9af.appspot.com",
  messagingSenderId: "1006779241413"
}

var instance = null;

class Auth {
  constructor(){
    this.fb = firebase.initializeApp(CONFIG);
    this.auth = this.fb.auth();
  }

  static getInstance(){
    if(!instance) instance = new Auth();
    return instance.auth;
  }

  static async emailAuth(email, password){
    return await Auth.getInstance().signInWithEmailAndPassword(email, password);
  }

  static async isLoggedIn(){
    return new Promise((resolve, reject)=>{
      Auth.getInstance().onAuthStateChanged((user)=>resolve(user!==null));
    });
  }

  static logout(){
    return Auth.getInstance().signOut();
  }
}
export default Auth;
