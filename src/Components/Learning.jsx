import React, {Component} from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';
import MobileStepper from '@material-ui/core/MobileStepper';
import { merge, zoomIn, zoomOut, fadeOut, shake } from 'react-animations';
import Radium, {StyleRoot} from 'radium';

import Question from './Views/Question';
import CardOption from './Views/CardOption';

const Logo = "https://cdn.kastatic.org/ka-perseus-images/45a518b64df15c3b0319e2b00b5f6fd4edb7f009.png"

const ANIM_EXIT = merge(fadeOut, zoomOut);

const ANIM_IN = {
  animation: 'x 0.3s',
  animationName: Radium.keyframes(zoomIn, 'zoomIn')
}

const ANIM_OUT = {
  animation: 'x 0.5s',
  animationName: Radium.keyframes(ANIM_EXIT, 'ANIM_EXIT')
}

const ANIM_ERR = {
  animation: 'x 0.5s',
  animationName: Radium.keyframes(shake, 'shake'),
}

const AvatarStyle = {
  width: 60,
  height:60
}

const CardStyle = {
  margin:"auto",
  marginTop: "2%",
  marginBottom: "5%",
  maxWidth:"1000px",
  float: "none"
}

class Learning extends Component {
  constructor(props){
    super(props);
    this.state = {
      stage: ANIM_IN
    }

  }

  resize=()=>this.forceUpdate();

  componentWillMount(){
    this.resize();
  }

  componentDidMount(){
    window.addEventListener('resize', this.resize);
  }

  componentWillUnMount(){
    window.removeEventListener('resize', this.resize);
  }

  exit=()=>{
    this.setState({stage:ANIM_OUT}, ()=>{
      setTimeout(()=>{
        this.setState({stage:{display:"none"}}, ()=>{
          window.location = "/"
        })
      }, 300);
    });
  }

  render(){
    return(
      <StyleRoot>
        <div style={this.state.stage}>
          <Card style={CardStyle}>
            <CardHeader title={
                <Grid container direction="row" justify="space-between">
                  <Grid item xs={6}>
                    <div className="menuTitle">Learning</div>
                  </Grid>
                  <Grid item xs={6} style={{textAlign:"right"}}>
                    <Button variant="outlined"
                            onClick={this.exit}>
                            <Icon>keyboard_arrow_left</Icon>
                            BACK
                    </Button>
                  </Grid>
                </Grid>
              }
              subheader={<span>Become the teacher of your robot.</span>}
              avatar={
                <img src={Logo} style={{width:100, height:"auto"}}/>
              }/>
            <CardContent>
              <Question questions={["1 x 4 = ", "2 x 4 = "]}/>
            </CardContent>
          </Card>

        </div>
      </StyleRoot>
    )
  }
}
export default Learning;


// <div style={{textAlign:"center", fontSize:"100px"}}>
//   1 x 4 = ?
// </div>
// <div style={{textAlign:"center", fontSize:"70px", marginTop:"28%"}}>
//   <Grid container direction="row" justify={window.innerWidth > 1000 ? "space-evenly" : "center"} alignItem="center" spacing={8}>
//     {
//       options.map((num)=>{
//         return (
//           <Grid item>
//             <CardOption content={num}/>
//           </Grid>
//         )
//       })
//     }
//   </Grid>
// </div>
// <MobileStepper
//   style={{marginTop:"2%", backgroundColor:"white"}}
//   variant="dots"
//   steps={6}
//   position="static"
//   activeStep={0}
//   nextButton={
//     <Fab style={{backgroundColor:"rgb(255, 121, 62)", color:"white"}}>
//       <Icon>keyboard_arrow_right</Icon>
//     </Fab>
//   }
//   backButton={
//     <Fab style={{backgroundColor:"rgb(255, 205, 182)", color:"white"}}>
//       <Icon>keyboard_arrow_left</Icon>
//     </Fab>
//   }
// />
