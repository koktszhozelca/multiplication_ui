import React, {Component} from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { merge, zoomIn, zoomOut, fadeOut, shake } from 'react-animations';
import Radium, {StyleRoot} from 'radium';

const Logo = "http://cartonomy.com/wp-content/uploads/2014/10/icon-setup.png"

const ANIM_EXIT = merge(fadeOut, zoomOut);

const ANIM_IN = {
  animation: 'x 0.3s',
  animationName: Radium.keyframes(zoomIn, 'zoomIn')
}

const ANIM_OUT = {
  animation: 'x 0.5s',
  animationName: Radium.keyframes(ANIM_EXIT, 'ANIM_EXIT')
}

const ANIM_ERR = {
  animation: 'x 0.5s',
  animationName: Radium.keyframes(shake, 'shake'),
}

const AvatarStyle = {
  width: 60,
  height:60
}

const CardStyle = {
  margin:"auto",
  marginTop: "2%",
  maxWidth:"1000px",
  float: "none"
}

class Settings extends Component {
  constructor(props){
    super(props);
    this.state = {
      stage: ANIM_IN
    }

  }

  resize=()=>this.forceUpdate();

  componentWillMount(){
    this.resize();
  }

  componentDidMount(){
    window.addEventListener('resize', this.resize);
  }

  componentWillUnMount(){
    window.removeEventListener('resize', this.resize);
  }

  exit=()=>{
    this.setState({stage:ANIM_OUT}, ()=>{
      setTimeout(()=>{
        this.setState({stage:{display:"none"}}, ()=>{
          window.location = "/"
        })
      }, 300);
    });
  }

  render(){
    return(
      <StyleRoot>
        <div style={this.state.stage}>
          <Card style={CardStyle}>
            <CardHeader title={
                <Grid container direction="row" justify="space-between">
                  <Grid item xs={6}>
                    <div className="menuTitle">Settings</div>
                  </Grid>
                  <Grid item xs={6} style={{textAlign:"right"}}>
                    <Button variant="outlined"
                            onClick={this.exit}>
                            <Icon>keyboard_arrow_left</Icon>
                            BACK
                    </Button>
                  </Grid>
                </Grid>
              }
              subheader={<span>Configure your group information.</span>}
              avatar={
                <img src={Logo} style={{width:100, height:"auto"}}/>
              }/>
          </Card>
        </div>
      </StyleRoot>
    )
  }
}
export default Settings;
