import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Main from './Components/Main';

class App extends Component {
  render() {
    return (
      <Main className="app"/>
    );
  }
}

export default App;
